# KickGAN

A simple DCGAN primer, aimed to musicians and deep learning beginners, to generate kick drum sounds in the raw audio domain.

It consists of a single Jupyter Notebook with a step-by-step explaination of the neural network implemented.
